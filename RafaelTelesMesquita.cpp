// Descrição do programa
/*
Objetivo: Receber dados de no mínimo 10 funcionários e apresentá-los na tela, 
    além de reajustar salário para funcionários que ganhem menos de 2 salários mínimos e calcular média de salários.
Entrada: Dados do funcionário, tais como: nome, endereço, e-mail, sexo e salário.
Saída: Impressão dos dados do funcionário, média dos salários e novo salário reajustado em 6% para
    funcionários que ganhem menos de 2 salários mínimos.
Autor: Rafael Teles Mesquita
Data: 19/06/2019
Hora: 21:54
*/

// Não tive a oportunidade de testar no 

// Adicionando biblioteca padrão da linguagem C
#include <stdlib.h>
// Adicionando biblioteca de Entrada/Saída de dados
#include <stdio.h>
// Adicionando bibliotecas para trabalhar com strings
#include <string.h>
#include <ctype.h>

// Valor Salário Mínimo
int SALARIO_MINIMO = 998;

// Funções auxiliares
float calcularMedia(float *salarios, int quantFuncionarios){
    // calculando a média de salários
    float mediaSalarios = 0;
    for(int i = 0; i < quantFuncionarios; i++){
        // printf("\ni: %d, salario: %f", i, salarioFuncionarios[i]);
        mediaSalarios += salarios[i] / quantFuncionarios;
    }  
    return mediaSalarios;
}
bool validaSexo(char siglaSexo, char* sexo){
    siglaSexo = tolower(siglaSexo);
    if(siglaSexo == 'm'){
        strcpy(sexo, "Masculino");
        return true;
    }
    if(siglaSexo == 'f'){
        strcpy(sexo, "Feminino");
        return true;
    }
    return false;
};

// Função principal do programa
int main(){
	// Declaração de variáveis
    int contador = 0;
    int quantFuncionarios;
    // Dados dos funcionários
	char nome[51], endereco[51], email[51], sexo[10];
    char siglaSexo;
    // Variáveis para armazenar salários e media de sálarios
    float salario;
    float mediaSalarios = 0;

    // Recebendo quantidade de funcionários
    printf("Informe a quantidade de funcionários(MIN: 10 funcionários):\n");
    scanf("%d", &quantFuncionarios);
    while (quantFuncionarios < 1){
        printf("Informe uma quantidade acima de 10 funcionários:\n");
        scanf("%d", &quantFuncionarios);
    }
    
    // Armazenar dados dos funcionarios
    char nomeFuncionarios[quantFuncionarios][51];
    char enderecoFuncionarios[quantFuncionarios][51];
    char emailFuncionarios[quantFuncionarios][51];
    char sexoFuncionarios[quantFuncionarios][10];
    float salarioFuncionarios[quantFuncionarios];
    int idFuncionarios[quantFuncionarios];	
	// Entrada de dados
    do{
        // Recebendo dados do funcionário
        printf("\nInforme os dados  do %dº funcionário\n", contador + 1);
        printf("Nome (MAX: 50 caracteres):\n");
        // Limpando o buffer do teclado
        // Preferi usar o 'setbuf' ao invés de 'fflush' porque desenvolvi o programa no mac.
        setbuf(stdin, NULL);
        scanf("%[^\n]s", nome);

        printf("Endereço (MAX: 50 caracteres):\n");
        setbuf(stdin, NULL);
        scanf("%[^\n]s", endereco);
        
        printf("E-mail (MAX: 50 caracteres):\n");
        setbuf(stdin, NULL);
        scanf("%[^\n]s", email);
        
        printf("Sexo (M para masculino ou F para feminino):\n");
        setbuf(stdin, NULL);
        /* 
            A função 'setbuf' não funcionou para consumir o \n no buffer do teclado, 
            utilizei o '%*c' para consumir esse \n.
        */ 
        scanf("%c%*c", &siglaSexo);
        
        // Validação da entrada de sexo
        while (!validaSexo(siglaSexo, sexo)){
            printf("Sexo inválido. Insira M para masculino ou F para feminino:\n");
            scanf("%c%*c", &siglaSexo);
        }

        printf("Salário:\n");
        scanf("%f", &salario);
        
        // Armazenando dados do funcionário
        strcpy(nomeFuncionarios[contador], nome);
        strcpy(enderecoFuncionarios[contador], endereco);
        strcpy(emailFuncionarios[contador], email);
        strcpy(sexoFuncionarios[contador], sexo);
        salarioFuncionarios[contador] = salario;
        idFuncionarios[contador] = contador + 1;

        // Incrementado contador
        contador++;
        // Pular uma linha
        printf("");
    } while (contador < quantFuncionarios);    

    // Ordenando os funcionários de acordo com o salário
    for (int i = 1; i < quantFuncionarios; i++) {
        for (int j = 0; j < i; j++) {
            if (salarioFuncionarios[i] > salarioFuncionarios[j]) {
                // Ordena percorrendo o vetor de salários e comparando os valores a partir de vetor[0] até vetor[quantFuncionarios]
                char nomeTemp[51];
                strcpy(nomeTemp, nomeFuncionarios[i]);
                strcpy(nomeFuncionarios[i], nomeFuncionarios[j]);
                strcpy(nomeFuncionarios[j], nomeTemp);

                char enderecoTemp[51];
                strcpy(enderecoTemp, enderecoFuncionarios[i]);
                strcpy(enderecoFuncionarios[i], enderecoFuncionarios[j]);
                strcpy(enderecoFuncionarios[j], enderecoTemp);

                char emailTemp[51];
                strcpy(emailTemp, emailFuncionarios[i]);
                strcpy(emailFuncionarios[i], emailFuncionarios[j]);
                strcpy(emailFuncionarios[j], emailTemp);

                char sexoTemp[10];
                strcpy(sexoTemp, sexoFuncionarios[i]);
                strcpy(sexoFuncionarios[i], sexoFuncionarios[j]);
                strcpy(sexoFuncionarios[j], sexoTemp);

                float salarioTemp;
                salarioTemp = salarioFuncionarios[i];
                salarioFuncionarios[i] = salarioFuncionarios[j];
                salarioFuncionarios[j] = salarioTemp;

                int idTemp;
                idTemp = idFuncionarios[i];
                idFuncionarios[i] = idFuncionarios[j];
                idFuncionarios[j] = idTemp;
            }
        }
    }

    // Calculando a média de salários
    mediaSalarios = calcularMedia(salarioFuncionarios, quantFuncionarios);

    // Imprimindo os dados dos jogadores em ordem decrescente de salário e recalculando o salário
    printf("\n**********************************************************************");
    printf("\n*Id*                   Dados dos Funcionários                        *");
    printf("\n**********************************************************************");
    for (int i = 0; i < quantFuncionarios; i++) {   
        if(idFuncionarios[i] < 10){
            printf("\n*0%d*", idFuncionarios[i]);
        } else {
            printf("\n*%d*", idFuncionarios[i]);
        }
        printf(" Nome:            * %s", nomeFuncionarios[i]);
        printf("\n*  * Endereço:        * %s", enderecoFuncionarios[i]);
        printf("\n*  * Email:           * %s", emailFuncionarios[i]);
        printf("\n*  * Sexo:            * %s", sexoFuncionarios[i]);
        printf("\n*  * Salário:         * %.2f", salarioFuncionarios[i]);
        printf("\n**********************************************************************");
        // Verificando se o salário é menor que dois salários nínimos
        if(salarioFuncionarios[i] < 2*SALARIO_MINIMO){
            // Aplicando reajuste de 6%
            salarioFuncionarios[i] *= 1.06;
        }
    }

    printf("\n* Média dos salários: * %.2f.", mediaSalarios);
    printf("\n**********************************************************************\n");

    // Ordenando os funcionários de acordo com o novo salário
    for (int i = 1; i < quantFuncionarios; i++) {
        for (int j = 0; j < i; j++) {
            if (salarioFuncionarios[i] > salarioFuncionarios[j]) {
                // Ordena percorrendo o vetor de salários e comparando os valores a partir de vetor[0] até vetor[quantFuncionarios]
                char nomeTemp[51];
                strcpy(nomeTemp, nomeFuncionarios[i]);
                strcpy(nomeFuncionarios[i], nomeFuncionarios[j]);
                strcpy(nomeFuncionarios[j], nomeTemp);

                char enderecoTemp[51];
                strcpy(enderecoTemp, enderecoFuncionarios[i]);
                strcpy(enderecoFuncionarios[i], enderecoFuncionarios[j]);
                strcpy(enderecoFuncionarios[j], enderecoTemp);

                char emailTemp[51];
                strcpy(emailTemp, emailFuncionarios[i]);
                strcpy(emailFuncionarios[i], emailFuncionarios[j]);
                strcpy(emailFuncionarios[j], emailTemp);

                char sexoTemp[10];
                strcpy(sexoTemp, sexoFuncionarios[i]);
                strcpy(sexoFuncionarios[i], sexoFuncionarios[j]);
                strcpy(sexoFuncionarios[j], sexoTemp);

                float salarioTemp;
                salarioTemp = salarioFuncionarios[i];
                salarioFuncionarios[i] = salarioFuncionarios[j];
                salarioFuncionarios[j] = salarioTemp;

                int idTemp;
                idTemp = idFuncionarios[i];
                idFuncionarios[i] = idFuncionarios[j];
                idFuncionarios[j] = idTemp;
            }
        }
    }

    // Calculando nova média de salários
    mediaSalarios = calcularMedia(salarioFuncionarios, quantFuncionarios);

    // Imprimindo os dados dos jogadores em ordem decrescente dos novos salários
    printf("\n**********************************************************************");
    printf("\n*Id*           Dados dos Funcionários(Novos Salários)                *");
    printf("\n**********************************************************************");
    for (int i = 0; i < quantFuncionarios; i++) {   
        if(idFuncionarios[i] < 10){
            printf("\n*0%d*", idFuncionarios[i]);
        } else {
            printf("\n*%d*", idFuncionarios[i]);
        }
        printf(" Nome:            * %s", nomeFuncionarios[i]);
        printf("\n*  * Endereço:        * %s", enderecoFuncionarios[i]);
        printf("\n*  * Email:           * %s", emailFuncionarios[i]);
        printf("\n*  * Sexo:            * %s", sexoFuncionarios[i]);
        printf("\n*  * Salário:         * %.2f", salarioFuncionarios[i]);
        printf("\n**********************************************************************");
    }

    printf("\n* Média dos salários: * %.2f.", mediaSalarios);
    printf("\n**********************************************************************\n");
    // Parando programa e retornando sucesso(0)
	return 0;
};
